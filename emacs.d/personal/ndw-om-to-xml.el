;;; ndw-om-to-xml --- For converting org to XML. -*- lexical-binding: t -*-

;;; Commentary:

;; Extensions for my personal Org environments.

;;; Code:

(require 'om)
(require 'om-to-xml)

(add-to-list 'om-to-xml-element-handlers
             '(special-block . ndw/om-special-block-to-xml))

(defun ndw/om-special-block-to-xml (elem)
  "Convert special block ELEM to XML based on type."
  (let* ((plist (cadr elem))
         (type (plist-get plist :type)))
    (cond
     ((string= type "MARGINNOTE")
      (ndw/om-marginal-note-to-xml elem))
     ((string= type "SIDENOTE")
      (ndw/om-marginal-note-to-xml elem "side-note"))
     ((string= type "SIDEBAR")
      (ndw/om-marginal-note-to-xml elem "sidebar"))
     ((string= type "PHOTO")
      (ndw/om-photo-to-xml elem))
     (t
      (ndw/om-base-element-to-xml elem)))))

(defun ndw/om-marginal-note-to-xml (elem &optional elemname)
  "Convert a marginal note special block ELEM to XML."
  (let* ((plist (cadr elem))
         (rest (cddr elem))
         (gi (if elemname elemname "marginal-note")))
    (if (member 'marginal-note om-to-xml-newline-before-start)
        (insert "\n"))
    (insert (concat "<" gi))
    (om-to-xml--plist-attributes plist '(:type))
    (insert ">")
    (om-to-xml--om-to-xml-list rest)
    (if (member 'marginal-note om-to-xml-newline-before-end)
        (insert "\n"))
    (insert (concat "</" gi ">"))))

(defun ndw/om-photo-to-xml (elem)
  "Convert a photo special block ELEM to XML."
  (let* ((plist (cadr elem))
         (rest (cddr elem))
         region-start region-end)
    (if (member 'photo om-to-xml-newline-before-start)
        (insert "\n"))
    (insert "<photo")
    (om-to-xml--plist-attributes plist '(:type))
    (insert ">")
    (setq region-start (point))
    (om-to-xml--om-to-xml-list rest)
    (if (member 'photo om-to-xml-newline-before-end)
        (insert "\n"))
    (setq region-end (point))
    (save-excursion
      (save-restriction
        (narrow-to-region region-start region-end)
        (goto-char (point-min))
        (replace-string "\n<paragraph>" "")
        (replace-string "\n</paragraph>" "")))
    (insert "</photo>")))

(provide 'ndw-om-to-xml)

;;; ndw-om-to-xml.el ends here
