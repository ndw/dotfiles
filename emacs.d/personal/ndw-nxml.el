;;; ndw-nxml --- My nxml-mode customizations. -*- lexical-binding: t -*-

;;; Commentary:

;; My customizations.

;;; Code:

;; Use my schemas.xml file, please
(defvar rng-schema-locating-files-default
      (list "schemas.xml" "~/schemas/schemas.xml" (expand-file-name "schema/schemas.xml" data-directory)))

(require 'nxml-mode)

(add-hook 'nxml-mode-hook 'rainbow-mode)

(setq rng-validate-chunk-size 4000)
(setq rng-validate-quick-delay 1)
(setq rng-validate-delay 2)

(setq nxml-slash-auto-complete-flag t)

(defun surround-region-with-tag (tag-name beg end)
  "Place TAG-NAME around region from BEG to END."
  (interactive "sTag name: \nr")
  (save-excursion
    (goto-char beg)
    (insert "<" tag-name ">")
    (goto-char (+ end 2 (length tag-name)))
    (insert "</" tag-name ">")))

(defcustom nxml-char-ref-display-glyph-omit '(10)
  "A list of Unicode code points.
The overlay character displayed after numeric character
references will be omitted for these values."
  :group 'nxml
  :type '(repeat integer))

(defun nxml-char-ref-display-extra (start end n)
  "I have no idea what this does between START and END using N."
  (when nxml-char-ref-extra-display
    (let ((name (or (get-char-code-property n 'name)
                    (get-char-code-property n 'old-name)))
          (glyph-string (and nxml-char-ref-display-glyph-flag
                             (not (member n nxml-char-ref-display-glyph-omit))
                             (char-displayable-p n)
                             (string n)))
          ov)
    (when (or name glyph-string)
      (setq ov (make-overlay start end nil t))
      (overlay-put ov 'category 'nxml-char-ref)
      (when name
        (overlay-put ov 'help-echo name))
      (when glyph-string
        (overlay-put ov
                     'after-string
                     (propertize glyph-string 'face 'nxml-glyph)))))))

(provide 'ndw-nxml)
;;; ndw-nxml ends here
