;;; ndw-mu4e --- My mu4e customizations. -*- lexical-binding: t -*-

;;; Commentary:

;; My hacks on mu4e

;;; Code:

(require 'mu4e)
(require 'org-mu4e)

;; elided

(provide 'ndw-mu4e)
;;; ndw-mu4e ends here
