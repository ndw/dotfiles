;;; ndw-fonts --- A collection of font-related items. -*- lexical-binding: t -*-

;;; Commentary:

;; Fonts and faces and related items.

;;; Code:

;; ======================================================================
;; From https://mstempl.netlify.com/post/beautify-org-mode/
(when (member "Symbola" (font-family-list))
  (set-fontset-font "fontset-default" nil
                    (font-spec :size 20 :name "Symbola")))

(when (member "Symbola" (font-family-list))
  (set-fontset-font t 'unicode "Symbola" nil 'prepend))
;; ======================================================================

(defvar ndw/base-font-size 180)
(defvar ndw/big-font-size (floor (* ndw/base-font-size 1.2)))
(defvar ndw/bigger-font-size (floor (* ndw/big-font-size 1.2)))
(defvar ndw/biggest-font-size (floor (* ndw/bigger-font-size 1.2)))
(defvar ndw/small-font-size (floor (/ ndw/base-font-size 1.2)))

(set-face-attribute 'default nil
                    :family "Operator Mono SSm Book" ;; :family "Iosevka"
                    :height ndw/base-font-size)

(set-face-attribute 'fixed-pitch nil
                    :family "Operator Mono SSm Book" ;; :family "Iosevka"
                    :height ndw/base-font-size)

(set-face-attribute 'variable-pitch nil
                    :family "Bookman Old Style"
                    :height ndw/base-font-size)

(set-face-attribute 'org-document-title nil
                    :height ndw/biggest-font-size)

(set-face-attribute 'org-level-1 nil
                    :height ndw/biggest-font-size)

(set-face-attribute 'org-level-2 nil
                    :height ndw/bigger-font-size)

(set-face-attribute 'org-level-3 nil
                    :height ndw/big-font-size)

(set-face-attribute 'org-level-4 nil
                    :height ndw/big-font-size)

(set-face-attribute 'org-meta-line nil
                    :height ndw/small-font-size)

(set-face-attribute 'org-block nil
                    :height ndw/base-font-size)

(set-face-attribute 'org-block-begin-line nil
                    :height ndw/small-font-size)

(set-face-attribute 'org-block-end-line nil
                    :height ndw/small-font-size)

(set-face-attribute 'org-document-info-keyword nil
                    :height ndw/small-font-size)

; variable width in text mode
(add-hook 'text-mode-hook
          (lambda ()
            (variable-pitch-mode 1)))

(add-hook 'nxml-mode-hook
          (lambda ()
            (variable-pitch-mode 0)))

(add-hook 'message-mode-hook
          (lambda ()
            (variable-pitch-mode 0)))

(add-hook 'yaml-mode-hook
          (lambda ()
            (variable-pitch-mode 0)))

(add-hook 'ledger-mode-hook
          (lambda ()
            (variable-pitch-mode 0)))

; syntax highlighting everywhere
(global-font-lock-mode 1)

(provide 'ndw-fonts)
;;; ndw-fonts ends here
