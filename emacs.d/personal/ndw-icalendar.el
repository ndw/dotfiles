;;; ndw-icalendar --- Functions related to icalendar. -*- lexical-binding: t -*-

;;; Commentary:

;; Random functions. Some that I've copied, some that I've written.

;;; Code:

(require 'icalendar)

(defun ndw--format-event (event)
  "Format EVENT ignoring my rdf: markup and other standard stuff."
  (let* ((xdesc  (or (icalendar--get-event-property event 'DESCRIPTION) ""))
         (rdfpos (string-match "^rdf:" xdesc))
         (f1     (if rdfpos
                     (let* ((prefix  (if (> rdfpos 0)
                                         (substring xdesc 0 (1- rdfpos))
                                       ""))
                            (rest    (substring xdesc (+ rdfpos 5)))
                            (dbln    (string-match "\n\n" rest))
                            (postfix (if dbln
                                         (substring rest (+ dbln 2))
                                       nil)))
                       (concat prefix (if postfix (concat " " postfix) "")))
                   xdesc))
         (dndpos (string-match "-- Do not delete or change any of the following text. --" f1))
         (desc   (concat "Desc: "
                         (if dndpos
                             (string-trim (substring f1 0 dndpos))
                           (string-trim f1))))
         (xsum    (icalendar--get-event-property event 'SUMMARY))
         (summary (if xsum xsum "???"))
         (xloc    (icalendar--get-event-property event 'LOCATION))
         (loc     (if xloc (concat " Loc: " xloc) ""))
         (xorg    (icalendar--get-event-property event 'ORGANIZER))
         (org     (if xorg (concat " Org: " xorg) "")))
    ;; For the purpose of my diary; I don't care about anything but the summary
    (format "%s%s%s%s" summary "" "" "")))

(setq icalendar-import-format 'ndw--format-event)

(provide 'ndw-icalendar)
;;; ndw-icalendar.el ends here
