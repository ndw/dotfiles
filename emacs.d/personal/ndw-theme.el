;;; ndw-theme --- My theme customizations. -*- lexical-binding: t -*-

;;; Commentary:

;; My customizations.

;;; Code:

(require 'poet-theme)
(require 'ndw-poet-theme)
(load-theme 'poet t t)
(load-theme 'poet-monochrome t t)
(load-theme 'poet-dark t t)
(load-theme 'poet-dark-monochrome t t)
(load-theme 'ndw-poet t t)

(custom-theme-set-faces
 'poet
 '(git-gutter:modified ((t (:foreground "#8D6E63"))))
 '(git-gutter:added ((t (:foreground "#288E1C"))))
 '(git-gutter:deleted ((t (:foreground "red"))))
 '(git-gutter:unchanged ((t (:foreground "cyan"))))
 '(org-block ((t (:background "#e0e0e0" :family "Operator Mono SSm Book"))))
 '(org-table ((t (:background "#e0e0e0" :family "Operator Mono SSm Book"))))
 '(header-line ((t :background "#e1d9c2")))
)

(custom-theme-set-faces
 'ndw-poet
 '(git-gutter:modified ((t (:foreground "#8D6E63"))))
 '(git-gutter:added ((t (:foreground "#288E1C"))))
 '(git-gutter:deleted ((t (:foreground "red"))))
 '(git-gutter:unchanged ((t (:foreground "cyan"))))
 '(org-block ((t (:background "#f0f0f0" :family "JetBrains Mono"))))
 '(org-table ((t (:background "#f0f0f0" :family "JetBrains Mono"))))
 '(header-line ((t :background "#fffff8")))
)

(custom-theme-set-faces
 'poet-dark
 '(git-gutter:modified ((t (:foreground "#8D6E63"))))
 '(git-gutter:added ((t (:foreground "#288E1C"))))
 '(git-gutter:deleted ((t (:foreground "red"))))
 '(git-gutter:unchanged ((t (:foreground "cyan"))))
 '(org-block ((t (:background "#e0e0e0" :family "Operator Mono SSm Book"))))
 '(org-table ((t (:background "#e0e0e0" :family "Operator Mono SSm Book"))))
 '(header-line ((t :background "#e1d9c2")))
)

(if (display-graphic-p)
    (enable-theme 'ndw-poet))

(provide 'ndw-theme)
;;; ndw-theme ends here
