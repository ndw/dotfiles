;;; ndw-org --- Functions related to Org-mode. -*- lexical-binding: t -*-

;;; Commentary:

;; Random functions. Some that I've copied, some that I've written.

;;; Code:

(require 'org)
(require 'org-tempo)
(require 'bh-org)

;; Change this to 'css to output class names instead
;; of inline CSS for htmlizing code.
(setq org-html-htmlize-output-type 'inline-css)

;; Don't output a wodge of CSS at the top of the file
(setq org-html-head-include-default-style nil)

(setq org-log-done 'time)
(setq org-ellipsis "↴")

;; My own hacks for presentations

(defun ndw/next-heading-p ()
  "Return non-nil if and only if there is a next heading."
  (interactive "p")
  (save-excursion
    (end-of-line)
    (re-search-forward
     (concat "^\\(?:" outline-regexp "\\)")
     nil 'move)))


(defun ndw/previous-heading-p ()
  "Return non-nil if and only if there is a previous heading."
  (interactive "p")
  (save-excursion
    (beginning-of-line)
    (re-search-backward
     (concat "^\\(?:" outline-regexp "\\)")
     nil 'move)))


(defun ndw/with-face (str &rest face-plist)
  (propertize str 'face face-plist))


(defun ndw/next-heading (arg)
  "Move to the next heading.
If there are no more headings and the region is narrowed, widen
the region and try again. ARG is ignored."
  (interactive "p")
  (if (ndw/next-heading-p)
      (progn
        (org-next-visible-heading 1)
        (org-narrow-to-subtree)
        (outline-hide-subtree)
        (outline-show-entry)
        (if (ndw/next-heading-p)
            (outline-show-children))
        (ndw/org-headline (not (looking-at "* ")))
        (recenter 0))
    (progn
      (widen)
      (if (ndw/next-heading-p)
          (progn
            (org-next-visible-heading 1)
            (org-narrow-to-subtree)
            (outline-hide-subtree)
            (outline-show-entry)
            (if (ndw/next-heading-p)
                (outline-show-children))
            (ndw/org-headline (not (looking-at "* ")))
            (recenter 0))
        (message "There's nothing next"))))
  (message nil))


(defun ndw/previous-heading (arg)
  "Move to the previous heading.
If there are no more headings and the region is narrowed, widen
the region and try again. ARG is ignored."
  (interactive "p")
  (if (ndw/previous-heading-p)
      (progn
        (org-previous-visible-heading 1)
        (org-narrow-to-subtree)
        (outline-hide-subtree)
        (outline-show-entry)
        (if (ndw/next-heading-p)
            (outline-show-children))
        (ndw/org-headline (not (looking-at "* ")))
        (recenter 0))
    (progn
      (widen)
      (if (ndw/previous-heading-p)
          (progn
            (org-previous-visible-heading 1)
            (org-narrow-to-subtree)
            (outline-hide-subtree)
            (outline-show-entry)
            (if (ndw/next-heading-p)
                (outline-show-children))
            (ndw/org-headline (not (looking-at "* ")))
            (recenter 0))
        (progn
          (beginning-of-buffer)
          (message "There's nothing previous")))))
  (message nil))


(defun ndw/quit-presentation (arg)
  "Quit the presentation mode. ARG is ignored."
  (interactive "p")
  (widen)
  (beginning-of-buffer)
  (setq header-line-format nil))

(defun ndw/start-presentation (arg)
  "Start the presentation mode. ARG is ignored."
  (interactive "p")
  (set-face-attribute 'org-document-info-keyword nil :height ndw/big-font-size :foreground "#BF360C")
  (set-face-attribute 'org-document-info nil :height ndw/bigger-font-size :foreground "#770b0b")
  (set-face-attribute 'org-meta-line nil :height ndw/big-font-size :foreground "#BF360C")
  (beginning-of-buffer)
  (setq mode-line-format nil)
  (setq header-line-format nil)
  (gfs/magnify-faces)
  (writegood-mode 0))

(defun ndw/org-headline-format (&optional slide)
  (org-with-wide-buffer
    (let ((slide-title
           (if (and slide (re-search-backward "^\\* \\(.*\\)$" nil t))
               (match-string-no-properties 1)))
          (pres-title
           (progn
             (beginning-of-buffer)
             (if (re-search-forward "^#\\+TITLE: \\(.*\\)$" nil t)
                 (match-string-no-properties 1)
               "Presentation has no title?"))))
      (if slide-title
          (concatenate 'string
           (ndw/with-face pres-title :inherit 'org-document-title)
           " / "
           (ndw/with-face slide-title :inherit 'org-level-1))
        (ndw/with-face pres-title :inherit 'org-document-title)))))


(defun ndw/org-headline (&optional slide)
  (setq header-line-format (ndw/org-headline-format slide)))

(org-defkey org-mode-map (kbd "C-c n") #'ndw/next-heading)
(org-defkey org-mode-map (kbd "C-c p") #'ndw/previous-heading)
(org-defkey org-mode-map (kbd "C-c q") #'ndw/quit-presentation)

;; /My own hacks for presentations

;; Only interpret _ and ^ as sub/superscripts if the
;; scripted content is in curly braces.
(setq org-use-sub-superscripts '{})
(setq org-export-with-sub-superscripts '{})
(setq org-special-ctrl-a/e t)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-switchb)

(set-face-attribute 'org-ellipsis nil
                    :underline nil)

;; Autosave org files when quitting the agenda or refiling
(advice-add 'org-agenda-quit :before
            (lambda (&rest _)
              (org-save-all-org-buffers)))
(advice-add 'org-refile :after
            (lambda (&rest _)
              (org-save-all-org-buffers)))
(add-hook 'org-capture-after-finalize-hook 'org-save-all-org-buffers)
(add-hook 'org-agenda-finalize-hook 'org-save-all-org-buffers)

;; Or just after thirty seconds
(add-hook 'auto-save-hook 'org-save-all-org-buffers)

;; Automatically tangle org files in various "literate" directory.
(defvar ndw/literate-directories
  (list (concat (getenv "HOME") "/Documents/Notes/literate/")
        (concat (getenv "HOME") "/Projects/devops/aws/src/")))

(defun ndw/tangle-literate ()
  "If the current file is in any 'literate' directories, code blocks are tangled automatically."
  (when (member (file-name-directory (directory-file-name buffer-file-name))
                ndw/literate-directories)
    (org-babel-tangle)
    (message "%s tangled" buffer-file-name)))
(add-hook 'after-save-hook #'ndw/tangle-literate)

(defun geo-to-openstreetmap (coords &optional zoom)
  "Parse COORDS into an OpenStreetMap link.
The format of COORDS is lat,long[/znum]. If ZOOM is specified, it
overrides znum."
  (let* ((geo (split-string coords "/" t "[\s]+"))
         (latlong (split-string (car geo) ",[\s\t]*" t "[\s]+")))
    (concat "https://www.openstreetmap.org/?mlat="
            (car latlong)
            "&mlon="
            (car (cdr latlong))
            "&zoom="
            (if zoom
                zoom
              (if (cdr geo)
                  (car (cdr geo))
                "17")))))

(setq org-link-abbrev-alist
      '(("geo"    . geo-to-openstreetmap)
        ("google" . "http://www.google.com/search?q=")
        ("github" . "https://github.com/%s")
        ("wiki"   . "https://en.wikipedia.org/wiki/%s")
        ("gmap"   . "http://maps.google.com/maps?q=%s")))

(setq org-agenda-include-diary t)
(setq org-agenda-span 10)
(setq org-deadline-warning-days 0)
(setq org-list-allow-alphabetical t)
(setq org-catch-invisible-edits 'show)
(setq org-src-preserve-indentation t)
(setq org-fontify-quote-and-verse-blocks t)
(setq org-return-follows-link nil)
(setq org-hide-emphasis-markers t)
(setq org-fontify-done-headline t)

;; Let's set inline images.
(setq org-display-inline-images t)
(setq org-redisplay-inline-images t)
(setq org-startup-with-inline-images "inlineimages")

;; ======================================================================
;; From https://mstempl.netlify.com/post/beautify-org-mode/
;; Pretty bulleted lists

(font-lock-add-keywords
 'org-mode
 '(("^ *\\([-]\\) "
    (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
(font-lock-add-keywords
 'org-mode
 '(("^ *\\([+]\\) "
    (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "◦"))))))

;; ======================================================================

; Although this is handy; it completely mangles filenames
; in Org files ☹
;;; From stackoverflow, the following incantation allows us to have
;;; parts of works emphasied with org-mode; e.g., /half/ed, ~half~ed,
;;; and right in the m*idd*le! Super cool stuff!
;(setcar org-emphasis-regexp-components " \t('\"{[:alpha:]")
;(setcar
; (nthcdr 1 org-emphasis-regexp-components) "[:alpha:]- \t.,:!?;'\")}\\")
;(org-set-emph-re
; 'org-emphasis-regexp-components org-emphasis-regexp-components)

;; Fontify the whole line for headings (with a background color).
(setq org-fontify-whole-heading-line t)

(setq org-refile-use-outline-path 'file)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-allow-creating-parent-nodes 'confirm)

;;; I put the Org files related to my agendas and TODO lists in
;;; ~/Dropbox/Org. I've commented all of that out so that this
;;; will build on a system without Dropbox.
;;;
;;; (setq org-directory "~/Dropbox/Org")
;;; 
;;; (setq org-agenda-files (quote ("~/Dropbox/Org/inbox.org"
;;;                                "~/Dropbox/Org/inbox-beorg.org"
;;;                                "~/Dropbox/Org/refile-beorg.org"
;;;                                "~/Dropbox/Org/marklogic.org"
;;;                                "~/Dropbox/Org/personal.org"
;;;                                "~/Dropbox/Org/someday.org"
;;;                                "~/Dropbox/Org/tickler.org"
;;;                                "~/Dropbox/Org/anniversary.org")))
;;; 
;;; (setq org-refile-targets '(("~/Dropbox/Org/marklogic.org" :maxlevel . 1)
;;;                            ("~/Dropbox/Org/personal.org" :maxlevel . 1)
;;;                            ("~/Dropbox/Org/someday.org" :maxlevel . 1)
;;;                            ("~/Dropbox/Org/tickler.org" :maxlevel . 2)))
;;;
;;; (setq org-default-notes-file "~/Dropbox/Org/inbox.org")

(setq org-directory "~/.emacs.d")
(setq org-agenda-files (quote ("~/.emacs.d/inbox.org")))
(setq org-refile-targets '(("~/.emacs.d/personal.org" :maxlevel . 1)))
(setq org-default-notes-file "~/.emacs.d/inbox.org")

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
        (sequence "WAITING(w@/!)" "SOMEDAY(s@/!)" "|" "CANCELED(c@/!)")))

(setq org-tag-alist
      '(("Balisage" . ?a)
        ("Cloud" . ?c)
        ("Emacs" . ?e)
        ("Meeting" . ?m)
        ("MarkLogic" . ?l)
        ("MDecl" . ?d)
        ("Shopping" . ?p)
        ("Travel" . ?t)
        ("XMLSS" . ?s)
        ("XProc" . ?x)))

;(setq org-enforce-todo-dependencies t)
;(setq org-agenda-dim-blocked-tasks t)
;(setq org-log-done 'note)

; Enable habit tracking (and a bunch of other modules)
; org-bbdb
(setq org-modules '(org-bibtex
                    org-crypt
                    org-gnus
                    org-id
                    org-info
                    org-habit
                    org-inlinetask
                    org-irc
                    org-mhe
                    org-protocol
                    org-w3m))

; global STYLE property values for completion
;(setq org-global-properties (quote (("STYLE_ALL" . "habit"))))

; position the habit graph on the agenda to the right of the default
(setq org-habit-graph-column 50)

(defun from-name (fromname fromaddress from)
  "Return the first non-empty match for FROMNAME FROMADDRESS and FROM."
  (nth 0
       (seq-filter '(lambda (s)
                      (not (string-empty-p s)))
                   (list fromname fromaddress from))))

(require 'seq)
(require 'doct)
(setq org-capture-templates
      (doct '(("Todo [inbox]" :keys "t"
               :file "~/Dropbox/Org/inbox.org"
               :headline "Tasks"
               :template ("* TODO %i%?"
                          "  :PROPERTIES:"
                          "  :CREATED: %U"
                          "  :SRC: %a"
                          "  :END:"))
              ("Reply [inbox]" :keys "r"
               :file "~/Dropbox/Org/inbox.org"
               :headline "Tasks"
               :template ("* TODO Reply to %:fromname re: %:subject"
                          "  SCHEDULED %t"
                          "  :PROPERTIES:"
                          "  :CREATED: %U"
                          "  :SRC: %a"
                          "  :END:"))
              ("Interesting" :keys "i"
               :file "~/Dropbox/Org/inbox.org"
               :headline "Interesting"
               :template ("* TODO %i%?"
                          "  :PROPERTIES:"
                          "  :CREATED: %U"
                          "  :SRC: %a"
                          "  :END:"))
              ("Tickler" :keys "T"
               :file "~/Dropbox/Org/tickler.org"
               :headline "Ticklers"
               :template ("* TODO %i%?"
                          "  :PROPERTIES:"
                          "  :CREATED: %U"
                          "  :SRC: %a"
                          "  :END:"))
              ("Respond" :keys "r"
               :file "~/Dropbox/Org/inbox.org"
               :headline "Tasks"
               :template ("* NEXT Respond to %:from on %:subject"
                          "SCHEDULED: %t"
                          "%U"
                          "%a"))
              ("Habit" :keys "h"
               :file "~/Dropbox/Org/inbox.org"
               :headline "Habits"
               :template ("* NEXT %?"
                          "%U"
                          "%a"
                          "SCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")"
                          ":PROPERTIES:"
                          ":STYLE: habit"
                          ":REPEAT_TO_STATE: NEXT"
                          ":END:")))))

(setq org-agenda-custom-commands
      (quote (("w" "Waiting and Postponed tasks" todo "WAITING|SOMEDAY"
               ((org-agenda-overriding-header "Waiting Tasks")))
              ("r" "Refile New Notes and Tasks" tags "LEVEL=1+REFILE"
               ((org-agenda-todo-ignore-with-date nil)
                (org-agenda-todo-ignore-deadlines nil)
                (org-agenda-todo-ignore-scheduled nil)
                (org-agenda-todo-ignore-timestamp nil)
                (org-agenda-overriding-header "Tasks to Refile")))
              ("n" "Next and Started tasks" tags-todo "-WAITING-CANCELED/!NEXT|STARTED"
               ((org-agenda-overriding-header "Next Tasks")))
              ("p" "Projects" tags-todo "LEVEL=2-REFILE|LEVEL=1+REFILE/!-DONE-CANCELED-WAITING-SOMEDAY-STARTED-NEXT"
               ((org-agenda-skip-function 'bh/skip-non-projects)
                (org-agenda-overriding-header "Projects")))
              ("A" "Tasks to be Archived" tags "LEVEL=2-REFILE/DONE|CANCELED"
               ((org-agenda-overriding-header "Tasks to Archive")
                (org-agenda-skip-function 'bh/skip-non-archivable-tasks)))

              ("X" "Export agenda to  iCal" agenda ""
               ((org-agenda-mode-hook
                 (lambda ()
                   (org-icalendar-combine-agenda-files)))))

              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-todo-ignore-with-date nil)
                (org-agenda-todo-ignore-scheduled nil)
                (org-agenda-todo-ignore-deadlines nil)
                (org-agenda-overriding-header "Habits")))
              ("#" "Stuck Projects" tags-todo "LEVEL=2-REFILE|LEVEL=1+REFILE/!-DONE-CANCELED"
               ((org-agenda-skip-function 'bh/skip-non-stuck-projects)
                (org-agenda-overriding-header "Stuck Projects")))

              ("c" "Simple agenda view"
               ((tags "PRIORITY=\"A\""
                      ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                       (org-agenda-overriding-header "High-priority unfinished tasks:")))
                (agenda ""
                        ((org-agenda-span 2)
                         (org-agenda-include-diary nil)))
                (alltodo ""
                         ((org-agenda-skip-function
                           '(or (air-org-skip-subtree-if-priority ?A)
                                (org-agenda-skip-if nil '(scheduled deadline))))))))

              ("*" "All open TODO tasks" tags-todo "-CANCELED"
               ((org-agenda-overriding-header "All Open TODO tasks")
                (org-agenda-todo-ignore-with-date nil)
                (org-agenda-todo-ignore-scheduled nil)
                (org-agenda-todo-ignore-deadlines nil)
                (org-agenda-todo-ignore-timestamp nil)
                (org-agenda-todo-list-sublevels t)
                (org-tags-match-list-sublevels 'indented))))))

;; https://blog.aaronbieber.com/2016/09/24/an-agenda-for-life-with-org-mode.html
(defun air-org-skip-subtree-if-priority (priority)
  "Skip an agenda subtree if it has a priority of PRIORITY.

PRIORITY may be one of the characters ?A, ?B, or ?C."
  (let ((subtree-end (save-excursion (org-end-of-subtree t)))
        (pri-value (* 1000 (- org-lowest-priority priority)))
        (pri-current (org-get-priority (thing-at-point 'line t))))
    (if (= pri-value pri-current)
        subtree-end
      nil)))

(setq org-tags-match-list-sublevels nil)

; Targets start with the file name - allows creating level 1 tasks
;(setq org-refile-use-outline-path (quote file))

; Targets complete in steps so we start with filename, TAB shows the next level of targets etc
;(setq org-outline-path-complete-in-steps t)

; Allow refile to create parent tasks with confirmation
;(setq org-refile-allow-creating-parent-nodes (quote confirm))

;; http://www.windley.com/archives/2010/12/capture_mode_and_emacs.shtml

(defadvice org-capture-finalize
  (after delete-capture-frame activate)
   "Advise capture-finalize to close the frame."
   (if (equal "capture" (frame-parameter nil 'name))
       (delete-frame)))

 (defadvice org-capture-destroy
  (after delete-capture-frame activate)
   "Advise capture-destroy to close the frame"
   (if (equal "capture" (frame-parameter nil 'name))
       (delete-frame)))

 ;; make the frame contain a single window. by default org-capture
 ;; splits the window.
 (add-hook 'org-capture-mode-hook
           'delete-other-windows)

 (defun make-capture-frame ()
   "Create a new frame and run org-capture."
   (interactive)
   (make-frame '((name . "capture")
                 (width . 120)
                 (height . 15)))
   (select-frame-by-name "capture")
   (setq word-wrap 1)
   (setq truncate-lines nil)
   (org-capture))

; http://orgmode.org/guide/Activation.html#Activation

; The following lines are always needed.  Choose your own keys.
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

; And add babel inline code execution
; babel, for executing code in org-mode.

(defun my-org-confirm-babel-evaluate (lang body)
  "Custom function for confirmation of babel evaluation.
If the LANG is not one of these, then ask before running BODY."
  (not (member lang '("ml-xquery" "bash" "sh" "plantuml" "xproc"))))

(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

(org-babel-do-load-languages
 'org-babel-load-languages
 ; load all language marked with (lang . t).
 '((C . t)
   (calc . t)
   (clojure . t)
   (css . t)
   (ditaa . t)
   (dot . t)
   (emacs-lisp . t)
   (gnuplot . t)
   (haskell . t)
   (io . t)
   (java . t)
   (js . t)
   (latex . t)
   (ledger . t)
   (lisp . t)
   (ocaml . t)
   (octave . t)
   (org . t)
   (perl . t)
   (picolisp . t)
   (plantuml . t)
   (python . t)
   (ref . t)
   (ruby . t)
   (sass . t)
;   (scala . t)
   (scheme . t)
   (screen . t)
   (shell . t)
   (sqlite . t)))

;; ======================================================================
;; See https://karl-voit.at/2016/12/23/my-org-region-to-property/

(defun org-read-entry-property-name ()
  "Read a property name from the current entry."
  (let ((completion-ignore-case t)
        (default-prop (or (and (org-at-property-p)
                               (org-match-string-no-properties 2))
                          org-last-set-property)))
    (org-completing-read
     (format "Property [%s]: " (if default-prop default-prop ""))
     (org-entry-properties nil nil)
     nil nil nil nil default-prop)))

(defun my-org-region-to-property (&optional property)
  "Copies the region as value to an Org-mode PROPERTY."
  (interactive)
  ;; if no region is defined, do nothing
  (if (use-region-p)
      ;; if a region string is found, ask for a property and set property to
      ;; the string in the region
      (let ((val (replace-regexp-in-string
                  "\\`[ \t\n]*" ""
                  (replace-regexp-in-string "[ \t\n]*\\'" ""
                                            (substring (buffer-string)
                                                       (- (region-beginning) 1)
                                                       (region-end))))
                 )
            ;; if none was stated by user, read property from user
            (prop (or property
                      (org-read-entry-property-name))))
        ;; set property
        (org-set-property prop val))))

;; ======================================================================
;; Org -> ical sync

(setq org-icalendar-store-UID t)
(setq org-icalendar-include-todo "all")
(setq org-icalendar-use-deadline '(event-if-not-todo event-if-todo))
(setq org-icalendar-use-scheduled '(todo-start))

;; ical -> Org  sync
;; From https://orgmode.org/worg/org-contrib/org-mac-iCal.html
(add-hook 'org-agenda-cleanup-fancy-diary-hook
          (lambda ()
            (goto-char (point-min))
            (save-excursion
              (while (re-search-forward "^[a-z]" nil t)
                (goto-char (match-beginning 0))
                (insert "0:00-24:00 ")))
            (while (re-search-forward "^ [a-z]" nil t)
              (goto-char (match-beginning 0))
              (save-excursion
                (re-search-backward "^[0-9]+:[0-9]+-[0-9]+:[0-9]+ " nil t))
              (insert (match-string 0)))))

;; ======================================================================

(defun my/org-drawer-format (name contents)
  "Export to HTML the drawers named with prefix ‘fold_’, ignoring case.

The resulting drawer is a ‘code-details’ and so appears folded;
the user clicks it to see the information therein.
Henceforth, these are called ‘fold drawers’.

Drawers without such a prefix may be nonetheless exported if their
body contains ‘:export: t’ ---this switch does not appear in the output.
Thus, we are biased to generally not exporting non-fold drawers.

One may suspend export of fold drawers by having ‘:export: nil’
in their body definition.

Fold drawers naturally come with a title.
Either it is specfied in the drawer body by ‘:title: ⋯’,
or otherwise the drawer's name is used with all underscores replaced
by spaces.
"
  (let* ((contents′ (replace-regexp-in-string ":export:.*\n?" "" contents))
         (fold? (s-prefix? "fold_" name 'ignore-case))
         (export? (string-match ":export:\s+t" contents))
         (not-export? (string-match ":export:\s+nil" contents))
         (title′ (and (string-match ":title:\\(.*\\)\n" contents)
                      (match-string 1 contents))))

    ;; Ensure we have a title.
    (unless title′ (setq title′ (s-join " " (cdr (s-split "_" name)))))

    ;; Output
    (cond
     ((and export? (not fold?)) contents′)
     (not-export? nil)
     (fold?
      (thread-last contents′
        (replace-regexp-in-string ":title:.*\n" "")
        (format "<details class=\"code-details\"> <summary> <strong>
            <font face=\"Courier\" size=\"3\" color=\"green\"> %s
            </font> </strong> </summary> %s </details>" title′))))))

(setq org-html-format-drawer-function 'my/org-drawer-format)

;; ======================================================================

(defun ndw/agenda-template ()
  "Insert an agenda template."
  (interactive)
  (let* ((repos    (string-match "/20[12][0-9]/[0-9][0-9]/[0-9][0-9]-" (buffer-file-name)))
         (date     (if repos
                       (let ((year  (string-to-number
                                     (substring (buffer-file-name) (1+ repos) (+ repos 5))))
                             (month (string-to-number
                                     (substring (buffer-file-name) (+ repos 6) (+ repos 8))))
                             (day   (string-to-number
                                     (substring (buffer-file-name) (+ repos 9) (+ repos 11)))))
                         (encode-time 0 0 12 day month year))
                     (current-time)))
         (lastmtg  nil) ;; date of last meeting
         (prev     (let ((weeks 1)
                         (minutes nil))
                     (while (and repos (< weeks 7)) ;; look back 6 weeks
                       (let* ((lastweek (time-add date (- (* 60 60 24 7 weeks))))
                              (lwd      (decode-time lastweek))
                              (pagenda  (concat
                                         (substring (buffer-file-name) 0 repos)
                                         (format "/%04d/%02d/%02d-" (nth 5 lwd) (nth 4 lwd) (nth 3 lwd))
                                         (substring (buffer-file-name) (+ repos 12)))))
                         (if (file-exists-p pagenda)
                             (progn
                               (setq lastmtg lastweek)
                               (setq minutes
                                     (let ((apos  (string-match "-agenda.org$" pagenda)))
                                       (if apos
                                           (concat (substring pagenda 0 apos) "-minutes.org")
                                         pagenda)))
                               (setq weeks 999))) ;; escape the loop
                         (setq weeks (1+ weeks))))
                     minutes))
         (title    (read-string "Meeting title: "))
         (tags     (read-string "Tags: "))
         (number   (if prev
                       (with-temp-buffer
                         (insert-file-contents prev)
                         (goto-char 0)
                         (if (re-search-forward "^#\\+MEETING: *")
                             (if (looking-at "[0-9]+")
                                 (match-string 0))))))
         (actions  (if prev
                     (with-temp-buffer
                       (insert-file-contents prev)
                       (goto-char 0)
                       (org-mode)
                       (let* ((ast  (org-element-parse-buffer))
                              (revh (org-element-map ast 'headline
                                      (lambda (hl)
                                        (and (string-equal (org-element-property :raw-value hl)
                                                           "Review of open action items")
                                             hl))
                                      nil t))
                              (newh (org-element-map ast 'headline
                                      (lambda (hl)
                                        (and (string-equal (org-element-property :raw-value hl)
                                                           "Summary of new action items")
                                             hl))
                                      nil t)))
                         (concat
                          ""
                          (if revh
                              (buffer-substring
                               (org-element-property :contents-begin revh)
                               (org-element-property :contents-end revh)))
                          (if newh
                              (buffer-substring
                               (org-element-property :contents-begin newh)
                               (org-element-property :contents-end newh))))))
                     "")))
  (insert "#+TITLE: ")
  (insert title)
  (insert ", ")
  (insert (format-time-string "%d %B %Y" date))
  (insert "\n")
  (insert "#+DATE: ")
  (insert (format-time-string "%Y-%m-%d" date))
  (insert "\n")
  (insert "#+AUTHOR: Norman Tovey-Walsh
#+STARTUP: showeverything\n")
  (if (not (string-equal "" tags))
      (progn
        (insert "#+FILETAGS: ")
        (dolist (elt (split-string tags))
          (progn
            (if (not (string-match-p "^:" elt))
                (insert ":"))
            (insert elt)))
        (if (not (string-match-p ":$" tags))
            (insert ":"))
        (insert "\n")))
  (insert "#+MEETING: ")
  (if number
      (insert (format "%03d" (1+ (string-to-number number))))
    (insert "???"))
  (insert "\n\n")
  (insert "* Agenda

** Administrivia

   + Roll call
   + Select a scribe
   + Accept the agenda?
   + Accept the minutes of the previous meeting")
  (if prev
      (progn 
        (insert ": ")
        (insert (format-time-string "%d %B %Y" lastmtg))))
  (insert "?
   + Next meeting: ")
  (insert (format-time-string "%d %B %Y" (time-add date (* 60 60 24 7))))
  (insert "

** Technical agenda
   + Review of open action items
   + Status updates
   + Any other business

** Review of open action items\n\n")
  (insert actions)
  (insert "
** Status updates

** Any other business

** Adjourned\n")
  (goto-char 0)
  ))

(defun ndw/minutes-template ()
  "Insert a minutes template."
  (interactive)
  (let* ((repos    (string-match "/20[12][0-9]/[0-9][0-9]/[0-9][0-9]-" (buffer-file-name)))
         (date     (if repos
                       (let ((year  (string-to-number
                                     (substring (buffer-file-name) (1+ repos) (+ repos 5))))
                             (month (string-to-number
                                     (substring (buffer-file-name) (+ repos 6) (+ repos 8))))
                             (day   (string-to-number
                                     (substring (buffer-file-name) (+ repos 9) (+ repos 11)))))
                         (encode-time 0 0 12 day month year))
                     (current-time)))
         (lastmtg  nil) ;; date of last meeting
         (prev     (let ((weeks 1)
                         (minutes nil))
                     (while (and repos (< weeks 7)) ;; look back 6 weeks
                       (let* ((lastweek (time-add date (- (* 60 60 24 7 weeks))))
                              (lwd      (decode-time lastweek))
                              (pagenda  (concat
                                         (substring (buffer-file-name) 0 repos)
                                         (format "/%04d/%02d/%02d-" (nth 5 lwd) (nth 4 lwd) (nth 3 lwd))
                                         (substring (buffer-file-name) (+ repos 12)))))
                         (if (file-exists-p pagenda)
                             (progn
                               (setq lastmtg lastweek)
                               (setq minutes
                                     (let ((apos  (string-match "-agenda.org$" pagenda)))
                                       (if apos
                                           (concat (substring pagenda 0 apos) "-minutes.org")
                                         pagenda)))
                               (setq weeks 999))) ;; escape the loop
                         (setq weeks (1+ weeks))))
                     minutes))
         (title    (read-string "Meeting title: "))
         (tags     (read-string "Tags: "))
         (number   (if prev
                       (with-temp-buffer
                         (insert-file-contents prev)
                         (goto-char 0)
                         (if (re-search-forward "^#\\+MEETING: *")
                             (if (looking-at "[0-9]+")
                                 (match-string 0))))))
         (actions  (if prev
                     (with-temp-buffer
                       (insert-file-contents prev)
                       (goto-char 0)
                       (org-mode)
                       (let* ((ast  (org-element-parse-buffer))
                              (revh (org-element-map ast 'headline
                                      (lambda (hl)
                                        (and (string-equal (org-element-property :raw-value hl)
                                                           "Review of open action items")
                                             hl))
                                      nil t))
                              (newh (org-element-map ast 'headline
                                      (lambda (hl)
                                        (and (string-equal (org-element-property :raw-value hl)
                                                           "Summary of new action items")
                                             hl))
                                      nil t)))
                         (concat
                          ""
                          (if revh
                              (buffer-substring
                               (org-element-property :contents-begin revh)
                               (org-element-property :contents-end revh)))
                          (if newh
                              (buffer-substring
                               (org-element-property :contents-begin newh)
                               (org-element-property :contents-end newh))))))
                     "")))
  (insert "#+TITLE: ")
  (insert title)
  (insert ", ")
  (insert (format-time-string "%d %B %Y" date))
  (insert "\n")
  (insert "#+DATE: ")
  (insert (format-time-string "%Y-%m-%d" date))
  (insert "\n")
  (insert "#+AUTHOR: Norman Tovey-Walsh
#+STARTUP: showeverything\n")
  (if (not (string-equal "" tags))
      (progn
        (insert "#+FILETAGS: ")
        (dolist (elt (split-string tags))
          (progn
            (if (not (string-match-p "^:" elt))
                (insert ":"))
            (insert elt)))
        (if (not (string-match-p ":$" tags))
            (insert ":"))
        (insert "\n")))
  (insert "#+MEETING: ")
  (if number
      (insert (format "%03d" (1+ (string-to-number number))))
    (insert "???"))
  (insert "\n\n")
  (insert "* Minutes

** Administrivia

   + Present: Norm, 
     + Regrets:
   + Scribe: Norm
   + Accept the agenda?
     + No objections
   + Accept the minutes of the previous meeting
     + No objections")
  (if prev
      (progn 
        (insert ": ")
        (insert (format-time-string "%d %B %Y" lastmtg))))
  (insert "?
   + Next meeting: ")
  (insert (format-time-string "%d %B %Y" (time-add date (* 60 60 24 7))))
  (insert "

** Technical agenda
   + Review of open action items
   + Status updates
   + Any other business

** Review of open action items\n\n")
  (insert actions)
  (insert "
** Status updates

** Any other business

** Adjourned\n")
  (goto-char 0)
  ))

(provide 'ndw-org)
;;; ndw-org ends here
