;;; ndw-ob-ml-marklogic --- My ob-ml-marklogic config. -*- lexical-binding: t -*-

;;; Commentary:

;; Configure ob-ml-marklogic.

;;; Code:

(require 'org)
(require 'ob-ml-marklogic)

(add-to-list 'org-src-lang-modes '("ml-xquery" . xquery))
(add-to-list 'org-src-lang-modes '("ml-javascript" . javascript))
(add-to-list 'org-src-lang-modes '("ml-sparql" . sparql))

;; Update org-babel-do-load-languages...
(add-to-list 'org-babel-load-languages
             '(ml-xquery . t))
(add-to-list 'org-babel-load-languages
             '(ml-javascript . t))
(add-to-list 'org-babel-load-languages
             '(ml-sparql . t))
(org-babel-do-load-languages
 'org-babel-load-languages
 org-babel-load-languages)

(setq ob-ml-common-default-header-args
  '((:ml-curl . "/usr/bin/curl")
    (:ml-host . "localhost")
    (:ml-scheme . "http")
    (:ml-port . 8000)
    (:ml-eval-path . "/v1/eval")
    (:ml-graphs-path . "/v1/graphs/sparql")
    (:ml-username . "admin")
    (:ml-password . "admin")
    (:ml-auth . "--digest")
    (:ml-output . "*ob-ml-marklogic output*")
    (:ml-save-output . t)))

;; See https://github.com/ndw/ob-ml-marklogic/issues/2

(setq org-babel-default-header-args:ml-xquery
  ob-ml-common-default-header-args)

(setq org-babel-default-header-args:ml-sparql
  ob-ml-common-default-header-args)

(setq org-babel-default-header-args:ml-javascript
  ob-ml-common-default-header-args)

(provide 'ndw-ob-ml-marklogic)
;;; ndw-ob-ml-marklogic ends here
