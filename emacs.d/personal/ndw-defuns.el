;;; ndw-defuns --- A miscellaneous collection of lisp functions. -*- lexical-binding: t -*-

;;; Commentary:

;; Random functions. Some that I've copied, some that I've written.

;;; Code:

;; For loading personal configurations
(defun personal (library)
  "Attempt to load elisp file LIBRARY; ignore errors if it doesn't work."
  (load (concat "~/.emacs.d/personal/" (symbol-name library)) 'noerror))

;; For loading libraries from the vendor directory
;; Modified from defunkt's original version to support autoloading.
;; http://github.com/defunkt/emacs/blob/master/defunkt/defuns.el
(defun vendor (library &rest autoload-functions)
  "Attempt to load vendor LIBRARY. AUTOLOAD-FUNCTIONS if requested."
  (let* ((file (symbol-name library))
         (normal (concat "~/.emacs.d/vendor/" file))
         (suffix (concat normal ".el"))
         (found nil))
    (cond
     ((file-directory-p normal) (add-to-list 'load-path normal) (set 'found t))
     ((file-directory-p suffix) (add-to-list 'load-path suffix) (set 'found t))
     ((file-exists-p suffix)  (set 'found t)))
    (when found
      (if autoload-functions
          (dolist (autoload-function autoload-functions)
            (autoload autoload-function (symbol-name library) nil t))
        (require library)))
    (personal library)))

;; Make the whole buffer pretty and consistent
(defun iwb()
  "Indent whole buffer."
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

;; Borrowed from http://whattheemacsd.com/key-bindings.el-01.html
(defun ndw/goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input."
  (interactive)
  (unwind-protect
      (progn
        (linum-mode 1)
        (goto-line (read-number "Goto line: ")))
    (linum-mode -1)))

;; Borrowed from http://superuser.com/q/603421/8424
(defun replace-smart-quotes (beg end)
  "Replace 'smart quotes' in buffer or (region from BEG to END) with ascii quotes."
  (interactive "r")
  (format-replace-strings '(("\x201C" . "\"")
                            ("\x201D" . "\"")
                            ("\x2018" . "'")
                            ("\x2019" . "'"))
                          nil beg end))

(defun yank-and-replace-smart-quotes ()
  "Yank (paste) and replace smart quotes from the source with ascii quotes."
  (interactive)
  (yank)
  (replace-smart-quotes (mark) (point)))

;; Utility functions by nwalsh
;; ======================================================================

(defun toggle-case-sensitivity ()
  "Toggle case sensitivity of search."
  (interactive)
  (message "Case sensitivity %s"
           (if (setq case-fold-search (not case-fold-search))
               "Off"
             "On")))

(defun toggle-linewrap ()
  "Toggle linewrap."
  (interactive)
  (message "Line Wrap %s"
           (if (setq truncate-lines (not truncate-lines))
               "Off"
             "On")))

(defun insert-pound ()
  "Insert a pound symbol."
  (interactive)
  (insert "£"))

(defun insert-euro ()
  "Insert a pound symbol."
  (interactive)
  (insert "€"))

(defun insert-zero-width-space ()
  "Insert a zero-width space.
In Org-mode, if you attempt to quote a verbatim string, for
example “~code~”, the markers for monospace aren't recognized in
that context. Putting a zero-widht space between the quotation
marks and the ~ or = symbols fixes that."
  (interactive)
  (insert "\u200B"))

;; ----------------------------------------------------------------------

(defun canonicalize-string (text)
  "Replace all tabs and newlines in TEXT with spaces; squash multiple spaces."
  ;; there's gotta be a better way...
  (let ((count 0))
    (while (< count (length text))
      (progn
        (if (or
             (= (string-to-char (substring text count (+ count 1))) ?\n)
             (= (string-to-char (substring text count (+ count 1))) ?\t))
            (setq text (concat
                        (substring text 0 count)
                        " "
                        (substring text (+ count 1)))))
        (setq count (+ count 1))))
    (if (string-match "^ +" text)
        (setq text (substring text (match-end 0))))
    (if (string-match " +$" text)
        (setq text (substring text 0 (string-match " *$" text))))
    (while (setq count (string-match "  +" text))
      (setq text (concat
                  (substring text 0 count)
                  " "
                  (substring text (match-end 0)))))
    text))

;; ----------------------------------------------------------------------

(defun string-wrap (string &optional width)
  "Wrap STRING into a lines of width no larger than WIDTH."
  (let ((line-list ())
        (line "")
        (count 0))
    (while (> (length string) width)
      (setq count width)
      (setq line (substring string 0 count))
      ;;(insert (format "(%s)\n" line))
      (while (and (> (length line) 0)
                  (not (string= (substring line -1 nil) " ")))
        (setq count (- count 1))
        (setq line (substring string 0 count)))
      (if (= (length line) 0)
          (progn
            (setq count width)
            (setq line (substring string 0 count)))
        (setq line (substring string 0 (- count 1))))
      ;;(insert (format "[%s]\n" line))
      (setq line-list (append line-list (list line)))
      (setq string (substring string count)))
    (append line-list (list string))))

;; ----------------------------------------------------------------------

(defun file-to-string (filename &optional trim)
  "Return the contents of the file FILENAME as a string.
If TRIM is t, all tabs and newlines are changed to spaces, all
multiple spaces are reduced to a single space, and all leading
and trailing spaces are removed."
  (let ((buf (generate-new-buffer " *buffer-to-string temp*"))
        (bufstring "")
        (newstring "")
        (char ""))
    (save-excursion
      (set-buffer buf)
      (insert-file-contents filename)
      (if trim
          (progn
            (perform-replace "[ \t\n]+" " " nil t nil)
            (goto-char (point-min))
            (perform-replace "^ *" "" nil t nil)
            (goto-char (point-min))
            (perform-replace " +$" "" nil t nil)))
      (goto-char (point-max))
      (setq bufstring (buffer-substring 1 (point)))
      (kill-buffer buf))
    bufstring))

(defun ndw/insert-date-time (with-tz)
  "Insert current date-time string in ISO 8601 format.
If WITH-TZ is nil, the time is printed without the timezone;
if WITH-TZ is t, the time is printed with the timezone;
if WITH-TZ has any other value, that value is interpreted as
a timezone and the current time in that timezone is inserted.
Example: 2010-11-29T23:23:35-08:00"
  (interactive)
  (let ((tz (cond ((eq with-tz nil) nil)
                  ((eq with-tz t) (current-time-zone))
                  (t (current-time-zone () with-tz)))))
    (insert
     (if (eq tz nil)
         (format-time-string "%Y-%m-%dT%T")
       (concat
        (format-time-string "%Y-%m-%dT%T" () tz)
        ((lambda (x) (concat (substring x 0 3) ":" (substring x 3 5)))
         (format-time-string "%z" () tz)))))))

(defun ndw/fill-paragraph (arg &optional region)
  "Fill the current paragraph with sensitivity to `visual-line-mode`.
In `visual-line-mode`, 'fill' the paragraph by effectively
unfilling it. That let's `visual-line-mode` do the wrapping. In the
absence of `visual-line-mode`, just fill the paragraph as usual. If
ARG is true, do the reverse. If REGION is provided, do the
filling to the paragraphs in the region."
  ;; Parts gleefully stolen from https://www.emacswiki.org/emacs/UnfillParagraph
  (interactive "P")
  (barf-if-buffer-read-only)
  (let ((unfill (and visual-line-mode (not arg))))
    (if unfill
        (let ((fill-column (point-max))
              ;; This would override `fill-column' if it's an integer.
              (emacs-lisp-docstring-fill-column t))
          (fill-paragraph nil region))
      (fill-paragraph nil region))))

(define-minor-mode ndw/adaptive-fill-paragraph-mode
  "Toggle adaptive fill paragraph mode.
When enabled, and when `visual-line-mode` is enabled,
`ndw/adaptive-fill-paragraph` fills a paragraph by 'unfilling'
it."
  :init-value nil)

(defun ndw/adaptive-fill-paragraph (orig-fun &rest args)
  (if (and visual-line-mode ndw/adaptive-fill-paragraph-mode)
      (let ((fill-column (point-max))
            ;; This would override `fill-column' if it's an integer.
            (emacs-lisp-docstring-fill-column t))
        (apply orig-fun args))
    (apply orig-fun args)))

(advice-add 'fill-region-as-paragraph :around #'ndw/adaptive-fill-paragraph)

(provide 'ndw-defuns)
;;; ndw-defuns.el ends here
